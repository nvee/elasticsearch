package com.lebo.elasticsearch.service.impl;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequest;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.MultiSearchResponse.Item;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.AdminClient;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.Requests;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.SortOrder;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lebo.elasticsearch.entity.Employee;
import com.lebo.elasticsearch.service.ElasticSearchService;
import com.lebo.elasticsearch.util.BeanUtil;

public class ElasticSearchServiceImpl implements ElasticSearchService{
	
	private static Logger LOG = LogManager.getLogger(ElasticSearchServiceImpl.class.getName());
	
	public ActionResponse singleCreate(Client client) throws Exception {
		IndexResponse response = client.prepareIndex("lebo", "employee").setSource(
				jsonBuilder()
					.startObject()
						.field("name","meisi")
						.field("age",32)
						.field("job","Fooballer")
						.field("birthday",new Date())
					.endObject())
		.get();
		LOG.info(JSONObject.toJSONString(response));
		//client.close();
		return response;
	}

	public ActionResponse bultCreate(Client client) throws Exception {
		BulkRequestBuilder bulkRequestBuilder = client.prepareBulk();
		for(int i=0;i<30;i++){
			bulkRequestBuilder.add(client.prepareIndex("lebo", "employee")
							  .setSource(JSON.toJSONString(Employee.createEmployee()),XContentType.JSON));
		}
		BulkResponse response = bulkRequestBuilder.get();
		LOG.info(JSONObject.toJSONString(response));
		//client.close();
		return response;
	}


	public ActionResponse singleDelete(Client client) throws Exception {
		//根据索引，类型，ID进行删除
		DeleteResponse response = client.prepareDelete("lebo", "employee", "AV9cl2pTk63NQ5us6Q2_").get();
		LOG.info(JSONObject.toJSONString(response));
		//client.close();
		return response;
	}

	public ActionResponse bultDelete(Client client) throws Exception {
		BulkByScrollResponse response = DeleteByQueryAction.INSTANCE.newRequestBuilder(client)
				//.filter(QueryBuilders.matchQuery("name", "liuganpu"))
				.filter(QueryBuilders.matchAllQuery())
				.source("lebo")
				.get();
		long deleteId = response.getDeleted();
		LOG.info("deleteID="+deleteId);
		//client.close();
		return response;
	}

	public ActionResponse update(Client client) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public ActionResponse upsert(Client client) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


	public ActionResponse BultOperation(Client client) throws Exception {
		BulkRequestBuilder builder = client.prepareBulk();
		//新增
		builder.add(client.prepareIndex("lebo", "employee").setSource(JSON.toJSONString(Employee.createEmployee()),XContentType.JSON));
		//修改
		builder.add(client.prepareUpdate("lebo", "employee", "AV9cl2pTk63NQ5us6Q2_")
				.setDoc(jsonBuilder()
						.startObject()
						.field("name", "updateNvee")
						.endObject()));
		//删除
		builder.add(client.prepareDelete("lebo", "employee", "AV9cl2pTk63NQ5us6Q2_"));
		BulkResponse response = builder.get();
		for(BulkItemResponse itemresponse:response.getItems()){
			LOG.info(JSON.toJSONString(itemresponse.getResponse()));
		}
		return response;
	}

	public void adminOperation(Client client) throws Exception {
		AdminClient adminClient = client.admin();
		ClusterHealthRequest request = new ClusterHealthRequest();
		ClusterHealthResponse response = adminClient.cluster().health(request).actionGet();
		LOG.info(JSONObject.toJSONString(response));
		
	}

	public void createIkAnalysis(Client client,String indexName,String typeName,Class<?> bean) throws Exception {
		List<JSONObject> fields = BeanUtil.ParseClazz(bean);
		
		XContentBuilder builder = jsonBuilder().startObject().startObject(typeName).startObject("properties");
		if(fields != null && !fields.isEmpty()){
			for(JSONObject field:fields){
				builder.startObject(field.getString("fieldName")).field(field.getString("fieldType"));
				if(field.getBooleanValue("isStore")){
					builder.field("store", "yes");
				}
				if(field.getBooleanValue("isIkAnalysis")){
					builder.field("analyzer","ik_smart");		
				}
				builder.endObject();
			}
		}
		builder.endObject().endObject();
		
	}

	public ActionResponse deleteIndex(Client client, String indexName) throws Exception {
		//判断索引是否存在
		IndicesExistsRequest inExistsRequest = new IndicesExistsRequest(indexName);
		IndicesExistsResponse inExistsResponse = client.admin().indices()
                .exists(inExistsRequest).actionGet();
		//存在则删除
		if(inExistsResponse != null && inExistsResponse.isExists()){
			DeleteIndexResponse dResponse = client.admin().indices().prepareDelete(indexName)
	                .execute().actionGet();
			LOG.info(JSONObject.toJSONString(dResponse));
			return dResponse;
		}
		return null;
	}

	public void createIndexMapping(Client client) throws Exception {
		//先创建空索引
		client.admin().indices().prepareCreate("lebo").execute().actionGet();
		//创建mapping映射
		XContentBuilder builder = jsonBuilder()
					.startObject()
						.startObject("employee")
							.startObject("properties")
								.startObject("name")
									.field("type","text")
									.field("index","not_analyzed")
									.field("store", "yes")
								.endObject()
								.startObject("age")
									.field("type","integer")
									.field("index","not_analyzed")
								.endObject()
								.startObject("job")
									.field("type","text")
									.field("index","not_analyzed")
								.endObject()
								.startObject("birthday")
									.field("type","date")
									.field("format","yyyy-MM-dd HH:mm:ss")
								.endObject()
								.startObject("interest")
									.field("type","text")
									.field("index","analyzed")
									.field("analyzer","ik_max_word")
								.endObject()
								.startObject("sex")
									.field("type","text")
									.field("index","not_analyzed")
								.endObject()
								.startObject("dept")
									.field("type","text")
									.field("index","not_analyzed")
								.endObject()
								.startObject("salary")
									.field("type","integer")
									.field("index","not_analyzed")
								.endObject()
							.endObject()
						.endObject()
					.endObject();
		 PutMappingRequest mappingRequest = Requests.putMappingRequest("lebo").type("employee").source(builder);
		 PutMappingResponse  response = client.admin().indices().putMapping(mappingRequest).actionGet();
		 LOG.info(JSONObject.toJSONString(response));
	}

}
