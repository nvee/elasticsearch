package com.lebo.elasticsearch.service.impl;

import java.util.Iterator;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.MultiSearchResponse.Item;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregator;
import org.elasticsearch.search.aggregations.bucket.global.Global;
import org.elasticsearch.search.aggregations.bucket.global.GlobalAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms.Bucket;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.NumericMetricsAggregation;
import org.elasticsearch.search.aggregations.metrics.sum.SumAggregationBuilder;
import org.elasticsearch.search.sort.SortOrder;

import com.alibaba.fastjson.JSONObject;
import com.lebo.elasticsearch.service.ElasticQueryService;

public class ElasticQueryServiceImpl implements ElasticQueryService{
	private static Logger LOG = LogManager.getLogger(ElasticQueryServiceImpl.class.getName());
	
	public ActionResponse simpleSearch(Client client) throws Exception {
		GetResponse response = client.prepareGet("lebo","employee","AV9cl2pTk63NQ5us6Q2_").get();
		LOG.info(JSONObject.toJSONString(response));
		client.close();
		return response;
	}

	public ActionResponse conditionSearch(Client client) throws Exception {
		SearchResponse response = client.prepareSearch("lebo")
				  .setTypes("employee")
				  .setQuery(QueryBuilders.matchAllQuery())
				  //.setQuery(QueryBuilders.matchQuery("name", "nvee"))
				  .setSearchType(SearchType.QUERY_THEN_FETCH)
				  //.setFrom(0)
				  //.setSize(20)
				  .addSort("age",SortOrder.DESC)
				  .get();
			SearchHit[] hits = response.getHits().getHits();
			for(SearchHit hit:hits){
				LOG.info(JSONObject.toJSONString(hit.getSource()));
			}
		client.close();
		return response;
	}
	public ActionResponse MultiSearch(Client client) throws Exception {
		MultiSearchRequestBuilder builder = client.prepareMultiSearch();
		//简单匹配查询
		builder.add(client.prepareSearch("lebo").setTypes("employee").setQuery(QueryBuilders.matchQuery("name", "nvee")));
		//数值范围查询
		builder.add(client.prepareSearch("lebo").setTypes("employee").setQuery(QueryBuilders.rangeQuery("age").gt(20).lt(30)));
		//日期范围查询
		builder.add(client.prepareSearch("lebo").setTypes("employee").setQuery(QueryBuilders.rangeQuery("birthday").from("1990-01-01").to("2010-01-01")));
		//前缀查询
		builder.add(client.prepareSearch("lebo").setTypes("employee").setQuery(QueryBuilders.prefixQuery("name", "pe")));
		//模糊查询
		builder.add(client.prepareSearch("lebo").setTypes("employee").setQuery(QueryBuilders.fuzzyQuery("interest", "secretary")));
		
		MultiSearchResponse response = builder.get();
		for(Item searsponse:response.getResponses()){
			SearchHits hits = searsponse.getResponse().getHits();
			for(SearchHit hit:hits.getHits()){
				LOG.info(hit.getSourceAsString());
			}
			LOG.info("\n");
		}
		return response;
	}

	public ActionResponse TermAggrSearch(Client client) throws Exception {
		SearchRequestBuilder builder = client.prepareSearch("lebo").setTypes("employee");
		
		TermsAggregationBuilder deptTerms = AggregationBuilders.terms("deptAgg").field("dept");
		TermsAggregationBuilder sexTerms  = AggregationBuilders.terms("sexAgg").field("sex");
		
		deptTerms.subAggregation(sexTerms);
		
		builder.addAggregation(deptTerms);
		
		SearchResponse response = builder.execute().actionGet();
		
		Map<String,Aggregation> aggMaps = response.getAggregations().asMap();
		
		StringTerms deptTermstr = (StringTerms) aggMaps.get("deptAgg");
		
		Iterator<Bucket> iters = deptTermstr.getBuckets().iterator();
		
		while(iters.hasNext()){
			Bucket bucket = iters.next();
			LOG.info("部门为:" + bucket.getKey() + " 有 " + bucket.getDocCount() + " 个人");
			
			StringTerms sexTermstr = (StringTerms)bucket.getAggregations().asMap().get("sexAgg");
			Iterator<Bucket> sexIters = sexTermstr.getBuckets().iterator();
			while(sexIters.hasNext()){
				Bucket sexbucket = sexIters.next();
				LOG.info("部门为:" + bucket.getKey() + " 性别为: " + sexbucket.getKey() + " 有 " +sexbucket.getDocCount() + " 个人");
			}
		}
		return response;
	}

	public ActionResponse MetricsAggregation(Client client) throws Exception {
		final String AggregationName = "AGG";
		NumericMetricsAggregation.SingleValue val;
		//均值聚合
		//AvgAggregationBuilder  builder = AggregationBuilders.avg(AggregationName).field("salary");
		//最小值
		//MinAggregationBuilder  builder = AggregationBuilders.min(AggregationName).field("salary");
		//
		//TopHitsAggregationBuilder builder = AggregationBuilders.topHits(AggregationName).sort("salary");
		SumAggregationBuilder  builder = AggregationBuilders.sum(AggregationName).field("salary");
		SearchResponse response = client.prepareSearch("lebo")
				.setTypes("employee")
				.setQuery(QueryBuilders.matchQuery("name", "张三"))
				.addAggregation(builder)
				.get();
		val = response.getAggregations().get(AggregationName);
		LOG.info(JSONObject.toJSONString(val.getValueAsString()));
		
		return response;
	}

	public ActionResponse TermFilterAggrSearch(Client client) throws Exception {
		SearchRequestBuilder builder = client.prepareSearch("lebo").setTypes("employee");
		GlobalAggregationBuilder globalbuilder = AggregationBuilders.global("agg");
		globalbuilder.subAggregation(AggregationBuilders.terms("deptAgg").field("dept"));
		
		AggregationBuilder aggregation =
			    AggregationBuilders
			        .filters("agg",
			            new FiltersAggregator.KeyedFilter("men", QueryBuilders.termQuery("dept", "men")),
			            new FiltersAggregator.KeyedFilter("women", QueryBuilders.termQuery("dept", "women")));
		globalbuilder.subAggregation(aggregation);
		builder.addAggregation(globalbuilder);
		SearchResponse response = builder.execute().actionGet();
		
		Global agg  = response.getAggregations().get("agg");
		
		LOG.info("agg [{}]",agg.getDocCount());
		/*for(Filters.Bucket bucket:agg.getBuckets()){
			String key = bucket.getKeyAsString();
			long docCount = bucket.getDocCount();
			LOG.info("key [{}], doc_count [{}]", key, docCount);
		}*/
		return response;
	}
}
