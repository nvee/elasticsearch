package com.lebo.elasticsearch.service;

import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.client.Client;

public interface ElasticQueryService {
	/**
	 * 
	 * @Description (简单查询)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse simpleSearch(Client client) throws Exception;
	/**
	 * 
	 * @Description (多条件查询)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse conditionSearch(Client client) throws Exception;
	/**
	 * 
	 * @Description (多任务复杂查询)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse MultiSearch(Client client) throws Exception;
	/**
	 * 
	 * @Description (trems聚合查询)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse TermAggrSearch(Client client) throws Exception;
	/**
	 * 
	 * @Description (Filter 聚合查询)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse TermFilterAggrSearch(Client client) throws Exception;
	
	/**
	 * 
	 * @Description (基本聚合查询操作)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse MetricsAggregation(Client client) throws Exception;
}
