package com.lebo.elasticsearch.service;

import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.client.Client;


public interface ElasticSearchService {
	/**
	 * 
	 * @Description (创建单个文档)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse singleCreate(Client client) throws Exception;
	/**
	 * 
	 * @Description (批量创建文档)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse bultCreate(Client client) throws Exception;
	/**
	 * 
	 * @Description (单个删除文档)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse singleDelete(Client client) throws Exception;
	/**
	 * 
	 * @Description (批量删除)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse bultDelete(Client client) throws Exception;
	/**
	 * 
	 * @Description (删除索引)
	 * @param client
	 * @param indexName 索引名称
	 * @return
	 * @throws Exception
	 */
	public ActionResponse deleteIndex(Client client,String indexName)throws Exception;
	/**
	 * 
	 * @Description (更新针对已有数据)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse update(Client client) throws Exception;
	/**
	 * 
	 * @Description (更新或者插入)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse upsert(Client client) throws Exception;
	
	/**
	 * 
	 * @Description (多任务复杂操作)
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public ActionResponse BultOperation(Client client) throws Exception;
	/**
	 * 
	 * @Description (针对集群的操作)
	 * @param client
	 * @throws Exception
	 */
	public void adminOperation(Client client) throws Exception;
	/**
	 * 
	 * @Description (创建索引映射)
	 * @param client
	 * @throws Exception
	 */
	public void createIndexMapping(Client client) throws Exception;
	
	public void createIkAnalysis(Client client,String indexName,String typeName,Class<?> t) throws Exception;
}
