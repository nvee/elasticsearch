package com.lebo.elasticsearch;

import org.elasticsearch.client.Client;

import com.lebo.elasticsearch.service.ElasticQueryService;
import com.lebo.elasticsearch.service.ElasticSearchService;
import com.lebo.elasticsearch.service.impl.ElasticQueryServiceImpl;
import com.lebo.elasticsearch.service.impl.ElasticSearchServiceImpl;
import com.lebo.elasticsearch.util.EsTool;
public class ElasticSearchDemo {
	
	public static void main(String[] args) {
		ElasticSearchService searchservice = new ElasticSearchServiceImpl();
		ElasticQueryService  queryService = new ElasticQueryServiceImpl();
		try {
			Client client = EsTool.CLIENT;
			//searchservice.bultCreate(client);
			queryService.TermFilterAggrSearch(client);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
} 
