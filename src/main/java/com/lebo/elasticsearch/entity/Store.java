package com.lebo.elasticsearch.entity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
public @interface Store {
	boolean value() default false;
}
