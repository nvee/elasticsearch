package com.lebo.elasticsearch.entity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
public @interface Analyzer {
	boolean value() default false;
}
