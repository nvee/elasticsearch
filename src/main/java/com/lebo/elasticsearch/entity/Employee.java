package com.lebo.elasticsearch.entity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import com.alibaba.fastjson.JSON;

public class Employee {
	private String name;
	
	private int age;
	
	private String job;
	
	private String birthday;
	
	private String interest;
	
	private String sex;
	
	private String dept;
	
	private int salary;
	
	public Employee() {
	
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.birthday = format.format(birthday);
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}
	
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}
	
	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public static Employee createEmployee(){
		String[] jobs = {"JAVA","WEB","C++","PYTHON","PERL","NODEJS","JAVASCRIPT","CSS","LINUX","TEST","EXCEL","WORD","LEADER"};
		String[] interest = {
			"我热爱生活，喜欢运动，呵呵呵呵",
			"天气这么好为什么不出去走走了，说不定有艳遇哦",
			"假装有很多兴趣爱好，其实我只想宅在家里睡大觉",
			"有空的时候看看书，或者看电影，听音乐",
			"不知道要做什么，或者可以打一局游戏",
			"编程使我快乐，happy coding ",
			"打篮球，踢足球羽毛球都是我喜爱的运动",
			"我只想发呆，别吵我，我想静静",
			"talk with others in english "
		};
		String[] sexs = {"man","woman"};
		
		String[] names = {"小米","张三","李四","王五","周六","kiate","loveping","amili","lula","nvee"};
		
		String[] depts = {"dept1","dept2","dept3"};
		Employee employee = new Employee();
		Random random = new Random();
		int age = random.nextInt(40);
		Calendar clendar = Calendar.getInstance();
		clendar.setTime(new Date());
		clendar.add(Calendar.YEAR, age*-1);
		clendar.set(Calendar.MONTH, random.nextInt(12));
		clendar.set(Calendar.HOUR_OF_DAY, random.nextInt(24));
		employee.setAge(age);
		employee.setInterest(interest[random.nextInt(interest.length)]);
		employee.setJob(jobs[random.nextInt(jobs.length)]);
		employee.setName(names[random.nextInt(names.length)]);
		employee.setBirthday(clendar.getTime());
		employee.setSex(sexs[random.nextInt(sexs.length)]);
		employee.setDept(depts[random.nextInt(depts.length)]);
		//int randNumber =rand.nextInt(MAX - MIN + 1) + MIN; // randNumber 将被赋值为一个 MIN 和 MAX 范围内的随机数
		employee.setSalary(random.nextInt(5000)+5000);
		return employee;
	}
	public static void main(String[] args) {
		for(int i=0;i<10;i++){
			System.out.println(JSON.toJSONString(Employee.createEmployee()));
		}
	}
}
