package com.lebo.elasticsearch.util;

import java.net.InetSocketAddress;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.SecureSetting;
import org.elasticsearch.common.settings.SecureSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.lebo.elasticsearch.conf.ConfigurationManager;




public class EsTool {
	
	private static Logger LOG = LogManager.getLogger(EsTool.class.getName());
	
	public final static  Client CLIENT = build();
	
	@SuppressWarnings("unchecked")
	public static Client build(){
		if(CLIENT != null){
			return CLIENT;
		}else{
			TransportClient client = null;
			try {
				String clusterName = ConfigurationManager.getProperty("cluster.name");
				String nodeListStr = ConfigurationManager.getProperty("cluster.node");
				
				String clusterLgName = ConfigurationManager.getProperty("cluster.loginname");
				String clusterPasswd = ConfigurationManager.getProperty("cluster.loginpasswd");
				
				LOG.info("start connecting cluster :" + clusterName + " ..............");
				
				Settings settings = Settings.builder()
							.put("cluster.name", clusterName)
							//xpack认证
							//.put("xpack.security.transport.ssl.enabled", false)
			                //.put("xpack.security.user", clusterLgName+":"+clusterPasswd)
							
							//客户端去嗅探整个cluster的状态，把集群中其它机器的ip地址加到客户端中，这样做的好处是一般你不用手动设置集群里所有集群的ip到连接客户端，它会自动帮你添加，并且自动发现新加入集群的机器。
							//但是至少添加一个InetSocketTransportAddress
			                //.put("client.transport.sniff", true)
							.build();
				
				client = new PreBuiltTransportClient(settings);
				
				String[] nodeList = nodeListStr.split(",");
				for(String node:nodeList){
					client.addTransportAddress(new InetSocketTransportAddress(new InetSocketAddress(node,9300))); 
				}
			} catch (Exception e) {
				LOG.error("Connecting cluster error ,please checking config.............", e);
			}
			return client;
		}
	}
	
	
}
