package com.lebo.elasticsearch.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.lebo.elasticsearch.entity.Analyzer;
import com.lebo.elasticsearch.entity.Store;

public class BeanUtil {
	public static List<JSONObject> ParseClazz(Class<?> clazz){
		List<JSONObject> lists = new ArrayList<JSONObject>();
		Field[] fields = clazz.getDeclaredFields();
		
		JSONObject obj;
		for(Field field:fields){
			obj = new JSONObject();
			obj.put("fieldName", field.getName());
			
			String fieldType = field.getType().getSimpleName();
			obj.put("fieldType", fieldType);
			
			Store store = field.getAnnotation(Store.class);
			obj.put("isStore", store.value());
			
			Analyzer analysis = field.getAnnotation(Analyzer.class);
			obj.put("isIkAnalysis", analysis.value());
			
			if("Date".equals(fieldType)){
				
			}
			lists.add(obj);
		}
		return lists;
	}

}
